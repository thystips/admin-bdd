CREATE USER IF NOT EXISTS 'replicant'@'%' identified by 'replicant_password';

GRANT REPLICATION SLAVE ON *.* TO 'replicant'@'%';

FLUSH PRIVILEGES;