# TP3-manager.sql
# Script exécuter avec l'utilisateur manager
# $ mysql -u manager -p < TP3-root.sql

USE teams;

INSERT INTO games (match_date, observations, victory)
VALUES ('2020-05-26', "Observation 1", 2),
       ('2020-07-02', "Observation 2", 5),
       ('2020-03-14', "Observation 2", 4);