# Pour la suite il faut se quitter et se connecter avec l'utilisateur `root`
# $ mysql -u root -p

DROP DATABASE IF EXISTS events;
CREATE DATABASE events;
USE events;

DROP TABLE IF EXISTS public_events;

CREATE TABLE public_events (
    event_date DATETIME,
    event_name VARCHAR(255),
    event_age_requirement INT
);

DROP TABLE IF EXISTS private_events;

CREATE TABLE private_events LIKE public_events;

DROP USER IF EXISTS event_manager;
DROP USER IF EXISTS event_supervisor;

CREATE USER 'event_manager'@'%' IDENTIFIED BY 'password';
CREATE USER 'event_supervisor'@'%' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON events.* TO 'event_manager'@'%';
GRANT SELECT ON events.public_events TO 'event_supervisor'@'%';

FLUSH PRIVILEGES;

# Pour la suite il faut se quitter et se connecter avec l'utilisateur `event_manager`
# $ mysql -u event_manager -p