# Pour la suite il faut se quitter et se connecter avec l'utilisateur `event_supervisor`
# $ mysql -u event_supervisor -p

USE events;
SELECT * FROM public_events;
SELECT * FROM private_events; # Fera une erreur

# Pour la suite il faut se quitter et se connecter avec l'utilisateur `root`
# $ mysql -u root -p