# Pour la suite il faut se quitter et se connecter avec l'utilisateur `event_manager`
# $ mysql -u event_manager -p

USE events;
INSERT INTO public_events (event_date, event_name, event_age_requirement)
VALUES ("2020-09-26 09:10:00","Ahah 1", 10),
       ("2020-09-27 09:05:00","Ahah 2", 12);
INSERT INTO private_events (event_date, event_name, event_age_requirement)
VALUES ("2020-09-26 23:00:00","Chut 1", 18),
       ("2020-09-26 22:10:00","Chut 2", 16)

# Pour la suite il faut se quitter et se connecter avec l'utilisateur `event_supervisor`
# $ mysql -u event_supervisor -p